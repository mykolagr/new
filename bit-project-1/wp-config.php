<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bit-project-1' );

/** MySQL database username */
define( 'DB_USER', 'bit-project-1' );

/** MySQL database password */
define( 'DB_PASSWORD', 'bit-project-1' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M<rYA]>p$a`|,<[nx}j*`z`YikQ$4%8e}=gO&TTN|m2DVR3guvYSK{+4rB.Ur}04' );
define( 'SECURE_AUTH_KEY',  '~B[G$7qfa] ,,&(=;p^6adnGO9&r5u7SnJi%2cgD:|Cg:r)-jzJz|$R}gZ|^O* j' );
define( 'LOGGED_IN_KEY',    '!Z7S(b$a_@-H!2kAdo<IZaQ[W0qQGzW`A_B-SZq;Him*{e_aL?*<F0$rf4EU:$a*' );
define( 'NONCE_KEY',        '=KlUL|G&WX+@ vpwQ(WQKNPt`l?{C+Tm>H Moq;BB=^+F7!a1jsT76Sy}vxTM8/!' );
define( 'AUTH_SALT',        'g/Xw[wqcyMA[8nBq2YJ@*%n**aHloQdPtN[l0q$&(p+IK{jXOIuH;n?%|IBlvba0' );
define( 'SECURE_AUTH_SALT', '0dS{h+4Ps(pp=yW?1ch`*@D*J[Xm-s6Hl<dRf]-OB<60@;ozUz4x#[hY}e)CwY~$' );
define( 'LOGGED_IN_SALT',   '*dZCTls_JhK_ME/Qc=i7t<%q[8dwohPb=k9fHfAlt02.rKI]D*z`IkNr;=+4ZeS3' );
define( 'NONCE_SALT',       '_10NyFuub_W@kE%]7$`]BME%gW/& n!!niw*&X}y=[@F;_o2srC[41[rB197CxtH' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

<?php

remove_action( 'wp_head', '_wp_render_title_tag', 1 );


function add_theme_scripts() {
    wp_enqueue_style( 'all-style', get_template_directory_uri() . '/dist/s/css/all.css' );

	wp_enqueue_script( 'all-script', get_template_directory_uri() . '/dist/s/js/main.js', array(), '1.0.0', true);

}
add_action ( 'wp_enqueue_scripts', 'add_theme_scripts' );




function setImage( $link , $alt , $title , $caption ) { ?>

	<figure>
		<picture>
			<img src="<?php echo $link; ?>" alt="<?php echo $alt; ?>" title="<?php echo $title; ?>">
		</picture>
		<?php if( $caption ){ ?>
			<figcaption>
				<?php echo $caption; ?>
			</figcaption>
		<?php } ?>
	</figure>

<?php }



add_filter('acf/load_field/key=field_6139dace0334c', 'load_just_categories_filter', 12, 1);
function load_just_categories_filter( $field ) {

    $field['taxonomy'] = [];
    $terms = get_terms( array(
        'taxonomy' => 'magazine',
        'hide_empty' => false,
		'hierarchical' => 1,
		'parent' => 0
    ) );




    if ( !empty($terms) ) {
        foreach( $terms as $term ) {
			
			
            $field['taxonomy'][] = 'magazine:'.$term->slug;
			
        }
    }

    return $field;
}






remove_filter( 'the_content', 'wpautop' );


function the_field_without_wpautop( $field_name ) {
	
	remove_filter('acf_the_content', 'wpautop');
	
	the_field( $field_name );
	
	add_filter('acf_the_content', 'wpautop');
	
}

function swipeks_admin_style() {
	wp_enqueue_style('admin-styles', '' . get_bloginfo('template_directory') . '/inc/admin/admin.css');
}
add_action('admin_enqueue_scripts', 'swipeks_admin_style');



function swipex_dd($data){
	echo '<pre>';
		var_dump($data);
	echo '</pre>';
}


function ft( $data ){

	if( strpos( $data , '</p>')  == true ){
		$order = ['<p>','</p>'];
		return strip_tags( $data , $order );

	} else {
		return $data;
	}
}





// contact-form 7
add_filter('wpcf7_autop_or_not', '__return_false');



add_filter('wpcf7_form_elements', function($content) {
    preg_match_all('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', $content,$matches);

    foreach($matches[2] as $match):
        $content = str_replace(trim($match),trim(preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $match)),$content);
    endforeach;
    return $content;
});







// Колонка миниатюры в списке записей админки
add_filter('manage_posts_columns', 'posts_columns', 5);
add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);
 
function posts_columns($defaults){
    $defaults['riv_post_thumbs'] = __('Миниатюра');
    return $defaults;
}
 
function posts_custom_columns($column_name, $id){
 if($column_name === 'riv_post_thumbs'){
        the_post_thumbnail( array(50, 50) );
    }
}


// user delete field 
function true_hide_contacts( $contactmethods ) {
	unset($contactmethods['linkedin']);
	unset($contactmethods['myspace']);
	unset($contactmethods['pinterest']);
	unset($contactmethods['soundcloud']);
	unset($contactmethods['twitter']);
	unset($contactmethods['tumblr']);
	unset($contactmethods['wikipedia']);
	unset($contactmethods['facebook']);
	unset($contactmethods['youtube']);
	unset($contactmethods['instagram']);
	unset($contactmethods['twitter']);
	return $contactmethods;
}
add_filter('user_contactmethods', 'true_hide_contacts', 10, 1);












if ( ! function_exists( 'swipeks_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function swipeks_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on swipeks, use a find and replace
		 * to change 'swipeks' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'swipeks', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

	

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		
		
	}
endif;
add_action( 'after_setup_theme', 'swipeks_setup' );






/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/navs.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/main.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



add_filter('excerpt_more', function($more) {
	return '...';
});


/**
 * Implement the Custom Admin
 */
require get_template_directory() . '/inc/admin/admin-dashboard.php';




// ACF options page(s)

if( function_exists('acf_add_options_page') ) {

	

	acf_add_options_page(array(

		'page_title' 	=> 'Global Settings',

		'menu_title'	=> 'Global Settings',

		'icon_url'      => 'dashicons-tide',

		'menu_slug' 	=> 'theme-general-settings',

		'capability'	=> 'edit_posts',

		'redirect'		=> false

	));

}



add_filter('acf/format_value/type=textarea', 'do_shortcode');






add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
  function add_default_value_to_image_field($field) {
    acf_render_field_setting( $field, array(
      'label'      => __('Default Image ID','acf'),
      'instructions'  => __('Appears when creating a new post','acf'),
      'type'      => 'image',
      'name'      => 'default_value',
    ));
  }


  add_action('acf/render_field_settings/type=audio_video_player', 'add_default_value_to_file_field', 20);
  function add_default_value_to_file_field($field) {
    acf_render_field_setting( $field, array(
      'label'      => __('Default File','acf'),
      'instructions'  => __('Appears when creating a new post','acf'),
      'type'      => 'audio_video_player',
      'name'      => 'default_value',
    ));
  }


add_filter('acf/format_value/type=wysiwyg', 'format_value_wysiwyg', 10, 3);
function format_value_wysiwyg( $value, $post_id, $field ) {
	$value = apply_filters( 'the_content', $value );
	return $value;
}


add_filter('the_content', 'do_shortcode');











// seo


add_action('admin_bar_menu', function() {
    remove_action('wp_before_admin_bar_render', 'wp_customize_support_script');
}, 50);




if( is_admin() ){
	// отключим проверку обновлений при любом заходе в админку...
	remove_action( 'admin_init', '_maybe_update_core' );
	remove_action( 'admin_init', '_maybe_update_plugins' );
	remove_action( 'admin_init', '_maybe_update_themes' );

	// отключим проверку обновлений при заходе на специальную страницу в админке...
	remove_action( 'load-plugins.php', 'wp_update_plugins' );
	remove_action( 'load-themes.php', 'wp_update_themes' );

	// оставим принудительную проверку при заходе на страницу обновлений...
	//remove_action( 'load-update-core.php', 'wp_update_plugins' );
	//remove_action( 'load-update-core.php', 'wp_update_themes' );

	// внутренняя страница админки "Update/Install Plugin" или "Update/Install Theme" - оставим не мешает...
	//remove_action( 'load-update.php', 'wp_update_plugins' );
	//remove_action( 'load-update.php', 'wp_update_themes' );

	// событие крона не трогаем, через него будет проверяться наличие обновлений - тут все отлично!
	//remove_action( 'wp_version_check', 'wp_version_check' );
	//remove_action( 'wp_update_plugins', 'wp_update_plugins' );
	//remove_action( 'wp_update_themes', 'wp_update_themes' );

	/**
	 * отключим проверку необходимости обновить браузер в консоли - мы всегда юзаем топовые браузеры!
	 * эта проверка происходит раз в неделю...
	 * @see https://wp-kama.ru/function/wp_check_browser_version
	 */
	add_filter( 'pre_site_transient_browser_'. md5( $_SERVER['HTTP_USER_AGENT'] ), '__return_empty_array' );
}


function galleryCount( $variable_galleries ){
	echo count($variable_galleries);
  }

register_sidebar(array(
	'name' => 'Multilanguage', // Отображаемое название области в панели управления
	'id' => 'multilanguage', // Уникальный ID области
	'description' => __( 'Header multilang'),
	'before_widget' => '<div class="select-group header__top-content-select">', // Начало обертки блока
	'after_widget' => '</div>', // Конец обертки блока
	'before_title' => '<h3 class="widget-title">', // Начало обертки заголовка
	'after_title' => '</h3>' // Конец обертки заголовка
));

function setIcon($field){
	$icon_ids = get_sub_field($field);
	$icon_args = array(
		'post_type' => 'icons',
		'posts_per_page' => 1,
		'order'          => 'ASC',
		'post_status'    => 'publish',
		'post__in'       =>  $icon_ids,
		'orderby'        => 'post__in',
	);
	$icons = get_posts($icon_args); 
				
		foreach( $icons as $icon ):  ?>
			<svg width="1em" height="1em" class="icon icon-effect-decor">
				<use xlink:href="<?=THEME?>/dist/s/images/useful/svg/theme/symbol-defs.svg#<?php echo get_field('acf_icon_id', $icon->ID); ?>"></use>
			</svg>
		<?php endforeach; ?>

		<?php wp_reset_postdata(); 

	}?>